package org.example;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.SecretOptions;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
import io.vertx.ext.jwt.JWTOptions;
import io.vertx.ext.web.Cookie;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.Session;
import io.vertx.ext.web.handler.*;
import io.vertx.ext.web.sstore.LocalSessionStore;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;


public class ServerVerticle extends AbstractVerticle {

    private static Map<String, String> userInfo = new HashMap<>();
    private static Session session;

    @Override
    public void start() throws Exception {

        userInfo.put("root", "root");
        userInfo.put("root2", "root2");
        Router router = Router.router(vertx);

        JWTAuth jwt = JWTAuth.create(vertx, new JWTAuthOptions()
                .addSecret(new SecretOptions()
                        .setType("HS256")
                        .setSecret("secret")
                )
        );

        router.route().handler(CookieHandler.create());
        router.route().handler(
                SessionHandler.create(LocalSessionStore.create(vertx)));
        AuthProvider ap = new AuthProviderImp();
        AuthHandler basicAuthHandler = BasicAuthHandler.create(ap);
        router.route("/api/*").handler(basicAuthHandler);
        router.route().handler(BodyHandler.create());


        router.post("/login").handler(ctx -> {

            ctx.addCookie(getCookie(ctx.session().id()));

            SignInRequest signInRequest = Json.decodeValue(ctx.getBody(), SignInRequest.class);
            if (userInfo.containsKey(signInRequest.getUsername())) {
                if (signInRequest.getPassword().equals(userInfo.get(signInRequest.getUsername()))) {
                    String passcode = passcodeEncryptor(signInRequest.getPassword());
                    session = ctx.session();
                    session.put("username", signInRequest.getUsername());
                    session.put("passcode", passcode);
                    ctx.response().putHeader("X-Frame-Options", "SAMEORIGIN")
                            .putHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                            .putHeader("Set-Cookie", getCSRFCookie());

                    ctx.response().end(passcode);
                } else {
                    ctx.response().setStatusCode(400);
                    ctx.response().end("Bad Credentials");
                }
            } else {
                ctx.response().setStatusCode(400);
                ctx.response().end("User does not exist");
            }
        });


        router.get("/api/newToken").handler(ctx -> {

            router.route().handler(CSRFHandler.create(getCSRFCookie()).setTimeout(1));
            ctx.response().putHeader("Content-Type", "text/plain");
            ctx.response().end(jwt.generateToken(new JsonObject(), new JWTOptions().setExpiresInSeconds(30).addAudience("http://localhost:8081/api/protected")
                    .setIssuer("http://localhost:8080/api/newtoken")));
        });


        vertx.createHttpServer().requestHandler(router).listen(8080);
    }


    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new ServerVerticle());
    }

    public static String getCSRFCookie() {
        return "X-XSRF-TOKEN" + "=" + getCookieValue() + "; path=/; SameSite=strict";
    }

    private static String getCookieValue() {
        SecureRandom secureRandom = new SecureRandom();
        byte[] buffer = new byte[50];
        secureRandom.nextBytes(buffer);
        return DatatypeConverter.printHexBinary(buffer);
    }

    public static Cookie getCookie(String sessionID) {
        return Cookie.cookie("JSESSIONID", sessionID);
    }

    public static Session getSession() {
        return session;
    }


    private String passcodeEncryptor(String password) {
        byte[] salt = new byte[0];
        try {
            salt = getSalt();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return getSecurePassword(password, salt);
    }

    private static byte[] getSalt() throws NoSuchAlgorithmException, NoSuchProviderException {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", "SUN");
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt;
    }

    private static String getSecurePassword(String passwordToHash, byte[] salt) {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(salt);
            byte[] bytes = md.digest(passwordToHash.getBytes());
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return generatedPassword;
    }

}
