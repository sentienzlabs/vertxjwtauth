package org.example;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.User;
import io.vertx.ext.web.Session;

public class AuthProviderImp implements AuthProvider {
    @Override
    public void authenticate(JsonObject jsonObject, Handler<AsyncResult<User>> handler) {
        Session session = ServerVerticle.getSession();
        if (jsonObject.getString("username").equals(session.data().get("username")) && jsonObject.getString("password").equals(session.data().get("passcode"))) {
            handler.handle(Future.succeededFuture());
        } else {
            handler.handle(Future.failedFuture("Failed"));
        }
    }
}
