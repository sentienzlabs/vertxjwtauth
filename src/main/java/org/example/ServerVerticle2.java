package org.example;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.ext.auth.SecretOptions;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.JWTAuthHandler;

public class ServerVerticle2 extends AbstractVerticle {
    @Override
    public void start() throws Exception {

        Router router = Router.router(vertx);

        JWTAuth jwt = JWTAuth.create(vertx, new JWTAuthOptions()
                .addSecret(new SecretOptions()
                        .setType("HS256")
                        .setSecret("secret"))
        );
        router.route("/api/*").handler(JWTAuthHandler.create(jwt));

        router.get("/api/protected").handler(ctx -> {
            ctx.response().putHeader("Content-Type", "text/plain");
            ctx.response().end("success");
        });


        vertx.createHttpServer().requestHandler(router).listen(8081);
    }


    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new ServerVerticle2());
    }
}



